from flask import (Flask, current_app, flash, redirect, render_template,
                   request, send_from_directory, url_for, g, url_for, session)

from flask_security import (RoleMixin, Security,
                            SQLAlchemySessionUserDatastore, UserMixin,
                            current_user, login_required)

from flask_sqlalchemy import SQLAlchemy

from werkzeug.utils import secure_filename

import datetime

now = datetime.datetime.now()


app = Flask(__name__)
application = app

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECURITY_PASSWORD_SALT'] = 'salt'
app.config['SECURITY_PASSWORD_HASH'] = 'sha512_crypt'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqlconnector://std_873:dimas1ck@std-mysql:3306/std_873'
db = SQLAlchemy(app)

app.secret_key = "super secret key"

################################################################################################


# создаем таблицу roles_users для названия и описания ролей
roles_users = db.Table(
    'roles_users',
    db.Column('id', db.Integer(), primary_key=True),
    db.Column('user_id', db.Integer(), db.ForeignKey('user.id', ondelete='CASCADE')),
    db.Column('role_id', db.Integer(), db.ForeignKey('role.id', ondelete='CASCADE'))
)

# создаем таблицу для пользователей
class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100))
    lastname = db.Column(db.String(100))
    patronymic = db.Column(db.String(50))
    password = db.Column(db.String(255))
    email = db.Column(db.String(100), unique=True)
    active = db.Column(db.Boolean())

    # динамическое добавление поля для таблицы Role **
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic')) 

    # инициализируем в список аргументов и именованый словарь наши поля
    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)

# таблица с ролями пользователей связанна с таблицами user и roles_users и заполняется автоматически **
class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    description = db.Column(db.String(255))




# Таблица журнала выдачи
class Journal(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE')) 
    book = db.Column(db.Integer, db.ForeignKey('books.id', ondelete='CASCADE'))
    status = db.Column(db.Integer, db.ForeignKey('status.id', ondelete='CASCADE'), default="1")

    # инициализируем в список аргументов и именованый словарь наши поля
    def __init__(self, *args, **kwargs):
        super(Journal, self).__init__(*args, **kwargs)

# Таблица Status
class Status(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.String(255))

# Таблица описания книги
class Books(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    author = db.Column(db.String(255)) 
    release = db.Column(db.Date())
    quantity = db.Column(db.Integer, default="1")

    # инициализируем в список аргументов и именованый словарь наши поля
    def __init__(self, *args, **kwargs):
        super(Books, self).__init__(*args, **kwargs)

db.create_all() # Создает все модели таблиц которые мы создали выше

# создаем дополнение user_datastore для Flask-security (Вход и регистрация)
user_datastore = SQLAlchemySessionUserDatastore(db.session, User, Role) 
security = Security(app, user_datastore) 
